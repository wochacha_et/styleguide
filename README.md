## 编码指南

#### 代码风格

为了更好的分工协作，前端代码编写请遵循以下规范：

* [HTML 代码规范](HTML.md)
* [CSS 代码规范](CSS.md)
* [JavaScript 代码规范](JavaScript.md)