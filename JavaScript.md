## JavaScript

### 基本设置

* 4 空格缩进
* UTF-8 编码

### 引号

使用单引号，这能能和HTML中属性的双引号区分开来。

### 空白与格式

在二元和三元运算符的符号与操作数之间添加空格，在非行末的`,` `;` `}`后添加空格， 在`{`前添加空格。并在每个逻辑块中间添加空白行。

```javascript
// 不推荐
var foo='bar',hello=foo+2,test=true;
function hi(){
 // ...
}
if(foo&&hello) {
// ...
}else if(foo){

}else if(!test){
// ...
}

// 推荐
var foo = 'bar';
var hello = foo + 2;
var test = true;

function hi(arg1, arg2) {
	// ...
}

if (foo && hello) {
// ...
} else if (foo) {
// ...
} else if (!test) {
// ...
}

```

### 注释
使用 `//`作为注释符，可以使用`/* */`作为多行注释符。注释符号与注释内容之间留空，注释的位置尽量放在代码之上：

```javascript

/*不推荐*/
//不推荐
; // 不推荐

/* 推荐 */
// 推荐
;

```

在今后要完善的代码中添加`// TODO`。

```javascript

if(true) {
	console.log('尚未实现');
	// TODO
}

```

### 不要为大括号另开一行

```javascript
// 不推荐
if (foo)
{
// ...
}

// 推荐
if (foo) {
// ...
}

// 只有一行语句时允许不带括号
if (foo) doSomething();
for (var i = 0; i < 10; i++) doSomething();

// 语句太长时允许写成两行，但建议写在同一行
for (var i = 0; i < 10; i++)
	doSomething(aaa, bb, cc, dd);

```

### 变量命名

使用小写字符开头的驼峰命名法：

```javascript
// 不推荐
var foo_bar = 'hello world';

// 推荐
var fooBar = 'hello world';

```

### 常量大写

```javascript
// 不推荐
var prefix = 'http://api.github.com/v1/';

// 推荐
var PREFIX = 'http://api.github.com/v1/';

```

### 使用字面量

```javascript
// 不推荐
var str = new String('str');
var obj = new Object();
var array = new Array();

// 推荐
var str = '';
var obj = {};
var array = [];

```

### 比较
建议使用`===`/`!==`而非`==`/`!=`

```javascript
// 不推荐
function foo(a) {
	if (a == 123) {
		// ...
	}
}

// 推荐
function foo(a) {
	a = Number(a);
	if (a === 123) {
		// ...
	} 
}

```

`==`的转换规则比较复杂

```javascript
var a = '';

// false
if (a === 0);

// true
if (a == 0)

```

对于可能不存在的全局引用可以先做下面的判断：

```javascript
if (typeof localStorage !== 'undefined') {
	// 此时访问localStorage就不会出现应用错误
}

或者

if ('localStorage' in self) {
	// 此时访问localStorage不会出现错误
}

```

但要注意它们之间的区别:

```
var a = undefined;

// true
// 判断一个全局变量是否声明
'a' in self;

// false
// 判断一个变量是否为undefined
typeof a !== 'undefined';

```

### var 声明

如果变量有初始赋值则使用单独的`var`:

```javascript
// 不推荐
var hello = 1, world = 2;

// 推荐
var hello = 1;
var world = 2;
var foo, fee, fxx;
```

### 函数定义

建议使用表达式来定义函数，而不是函数语句。

```javascript
// 不推荐
function fee() {
 // ...
}

// 推荐
var foo = function() {
	// ...
}

```
因为函数语句在进入作用域时声明，破坏了程序从上往下的执行顺序。

```javascript
void function() {
	foo();
	
	return null;
	
	function foo() {
		// ...
	}
}();
```
只引用一次的函数建议匿名定义，因为名称存在主观因素。

```javascript
	// 不推荐
	var foo = function() {
		// ...
	}
	
	element.onclick = foo;
	
	// 推荐
	element.onclick = function() {
		// ...
	}

```

### 自执行函数

```javascript
// 不推荐
(function() {
	// ...
})();

+function() {
 // ...
}();

// 推荐
;(function() {

})();
```

### 禁止事项

* 禁止使用`eval`, 非用不可时可以使用`Function`构造函数替代
* 禁止使用`with`
* 禁止在块作用域中使用函数声明语句。