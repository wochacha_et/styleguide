## CSS
### 基本设置

* 4 空格缩进
* UTF-8 编码

### 空白

* 大括号与选择器之间留空，冒号后面留空，注释内外前后留空。

```css
/* 我是注释 */

div { /* 我是注释 */ }

span {
    color: red; /* 我是注释 */
}
```

* 只有一条样式时允许和选择器写到同一行（不强制分开写三行）。
* 一个选择器中有多个样式声明时每条写一行
* 多个选择器使用逗号隔开时写在不同的行，大括号不要另起一行。

```css

div,
span {
	color: red;
	font-size: 12px;
}

```

* 所有最外层引号使用双引号
* 使用逗号分隔的多个样式值写成多行

```css

.block {
	box-shadow: 0 0 0 rgba(#000, 0.1),
				1px 1px 0 rgba(#000, 0.2),
				2px 2px 0 rgba(#000, 0.3),
				3px 3px 0 rgba(#000, 0.4),
				4px 4px 0 rgba(#000, 0.5);
}

```

### 分号

分号在CSS中不仅是分隔符，更应该是单条样式的结束符。

```css
/* 不好的 */
div {
	color: red
}

/* 推荐 */
div {
	color: red;
}
```

### 盒模型

理想状况下，整个文档中都要使用统一的盒模型。可以通过`* { box-sizing: border-box; }`来全局定义，但是尽量不要每个元素的默认盒模型设置。

```css
/* 不好的 */
div {
	width: 100%;
	padding: 10px;
	box-sizing: border-box;
}
/* 推荐 */
div {
	padding: 10px;
}
```

### 文档流
尽量不要改变元素的默认行为，而且尽量让元素处于正常的文档流之中。例如，要移除image下的空白不应该通过改变它默认的`display`属性：

```css
/* 不好的 */
img {
	display: block;
}

/* 推荐 */
img {
	vertical-align: middle;
}

```

同样，尽量不要元素脱离正常的文档流。

```css
/* 不好的 */
div {
	width: 100px;
	position: absolute;
	right: 0;
}
/* 推荐 */
div {
	width: 100px;
	margin-left: auto;
}
```
### 定位

CSS中可以有很多方式来控制元素的位置，按照性能排序如下：

```css
display: block;
display: flex;
position: relative;
position: sticky;
position: absolute;
position: fixed;
```
### 选择器

选择器与DOM结构的耦合度要尽量低。当选择器超过3层时，可以考虑为要选择的元素添加类名。

```css
/* 不好的 */
div:first-of-type :last-child >p ~ *

/* 推荐 */
div:first-of-type .info

```

避免重复强调你的选择器

```css
/* 不好的 */
img[src$=svg], ul > li:first-child {
	opacity: 0;
}

/* 推荐 */
[src$=svg], ul > :first-child {
	opacity: 0;
}
```
### 优先级

减少`id`的使用，避免使用`!important`。

```css
/* 不好的 */
.bar {
	color: green !important;
}
.foo {
	color: red;
}

/* 推荐 */
.foo.bar {
	color: green;
}
.foo {
	color: red;
}
```
### 覆盖

覆盖之前的样式声明会使调试变得复杂，如果可能尽量避免这么做。

```css
/* 不好的 */
li {
	visibility: hidden;
}
li:first-child {
	visibility: visible;
}

/* 推荐 */
li + li {
	visibility: hidden;
}
```

### 继承

不要重复声明可以继承的样式。

```css
/* 不好的 */
div h1, div p {
	text-shadow: 0 1px 0 #fff;
}
/* 推荐 */
div {
	text-shadow: 0 1px 0 #fff;
}
```

### 简洁

保持代码简洁。使用简写属性，避免使用多个属性。

```css
/* 不好的 */
div {
	transition: all 1s;
	top: 50%;
	margin-top: -10px;
	padding-top: 5px;
	padding-right: 10px;
	padding-bottom: 20px;
	padding-left: 10px;
}

/* 推荐 */

div {
	transition: 1s;
	top: calc(50% - 10px);
	padding: 5px 10px 20px;
}

```

### 语义

使用语义化的表示而不是数学公式。

```css
/* 不好的 */
:nth-child(2n+1) {
	transform: rotate(360deg);
}
/* 推荐 */
:nth-child(odd) {
	transform: rotate(1turn);
}

```
### 厂商前缀

如果要使用带浏览器厂商前缀的属性，要把它们放到标准属性声明之前。

```css
/* 不好的 */
div {
	transform: scale(2);
	-webkit-transform: scale(2);
	-moz-transform: scale(2);
	-ms-transform: scale(2);
	transition: 1s;
	-webkit-transition: 1s;
	-moz-transition: 1s;
	-ms-transition: 1s;
}
/* 推荐 */
div {
	-webkit-transform: scale(2);
	transform: scale(2);
	transition: 1s;
}

```

### 动画

如果能使用transitions完成的事情就不要使用animations。而且animate也要尽量限定在`opacity`和`transform`属性。

```css
/* 不好的 */
div:hover {
	animation: move 1s forwards;
}

@keyframes move {
	100% {
		margin-right: 100px;
	}
}
/* 推荐 */
div:hover {
	transition: 1s;
	transform: translateX(100px);
}
```

### 单位

尽量使用不带单位的数值，而且相对单位也尽量用`rem`，能使用秒的地方就不要用毫秒。

```css
/* 不好的 */
div {
	margin: 0px;
	font-size: .9em;
	line-height: 22px;
	transition: 500ms;
}

/* 推荐 */
div {
	margin: 0;
	font-size: .9rem;
	line-height: 1.5;
	transition: .5s;
}
```

### 颜色

如果要用到透明，那就用`rgba`。其他情况下，都尽量使用十六进制格式。

```css
/* 不好的 */
div {
	color: hsl(103, 54%, 43%);
}
/* 推荐 */
div {
	color: #5a3;
}
```

### 绘图

如果能通过CSS绘制一些图形，就不要通过HTTP请求从服务器获取图片。

```css
/* 不好的 */
div::before {
	content: url(white-circle.svg);
}
/* 推荐 */
div::before {
	content: "";
	display: block;
	width: 20px;
	height: 20px;
	border-radius: 50%;
	background: #fff;
}
```

### Hacks

不要使用。

```css
/* 不好的 */
div {
	// position: relative;
	transform: translateZ(0);
}
/* 推荐 */
div {
	/* position */
	will-change: transform;
}
```