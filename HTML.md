## HTML
### 基本设置

* 4 空格缩进
* UTF-8 编码

### 统一符号

* 所有标签和属性名称一律小写
* 属性值一律使用双引号

```html
<!-- 不推荐 -->
<DIV>
	<span CLASS='Test'>测试</span>
</DIV>

<!-- 推荐 -->
<div>
  <span class="test">测试</span>
</div>
```

### 可省略部分

* 在没有特殊需求的情况下，不省略可选的结束标签

* 不省略可选的自结束标签末尾的斜杠

* 建议自结束标签包含属性时在结束的斜杠前面添加空格

```html
<input type="text" />
<hr />
```

* 建议无值属性不写等号和值

```html
<input type="checkbox" checked />
<script src="..." async defer></script>
```

### 语义化

HTML5中新增了很多语义化的标签，为我们精确描述内容提供了帮助。

```html
<!-- 不推荐 -->
<div id="main">
	<div class="article">
		<div class="header">
			<h1>Blog Post</h1>
			<p>Published: <span>21st Feb, 2015</span></p>
		</div>
		<p>...</p>
	</div>
<div>

<!-- 推荐 -->
<main>
	<article>
		<header>
			<h1>Blog Post</h1>
			<p>Published: <time datetime="2015-02-21">21st Feb, 2015</time></p>
		</header>
	</article>
</main>
```

要明确所使用标签的含义。错误的使用语义化标签会带来更坏的影响。

```html
<!-- 不推荐 -->
<h1>
	<figure>
		<img alt="Company" src="logo.png">
	</figure>
</h1>

<!-- 推荐 -->
<h1>
	<img alt="Company" src="logo.png">
</h1>
```

### 简洁

保证代码简洁，不要再遵循老旧的XHTML的规范。

```html
<!-- 不好的 -->
<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>Contact</title>
		<link rel="stylesheet" href="style.css" type="text/css"/>
	</head>
	<body>
		<h1>Contact Me</h1>
		<label>
			Email address:
			<input type="email" placeholder="you@email.com" required="required" />
		</label>
		<script src="main.js" type="text/javascript"></script>
	</body>
</html>

<!-- 推荐 -->
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Contact</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<h1>Contact Me</h1>
		<label>
			Email Address:
			<input type="email" placeholder="you@email.com" required>
		</label>
		<script src="main.js"></script>
	</body>
</html>
```

### 可访问性
网站的可访问性应该是头等要考虑的大事。要增强网站的可访问性，你不需要成为WCAG专家，只要从小事做起，就可以形成大的改变，例如：

* 开始正确的使用`alt`属性
* 正确使用链接和按钮，不要再用`<div class="button">`这种形式
* 不要完全依靠颜色来传达信息
* 更明显的表示控件的标签

```html
<!-- 不好的 -->
<h1><img alt="logo" src="logo.png"></h1>

<!-- 推荐 -->
<h1><img alt="My Company, Inc." src="logo.png"></h1>
```

### 语言和编码

虽然HTML5规范并没有强制要求定义文档的语言和编码，但仍强烈推荐在文档开始就定义它们，即使在HTTP请求的headers里已经指定了。UTF-8胜过其他任何的编码方式。

```html
<!-- 不好的 -->
<!doctype html>
	<title>Hello, world.</title>

<!-- 推荐 -->
<!doctype html>
<html lang="en">
	<meta charset="utf-8">
	<title>Hello, world.</title>
</html>

```

### 性能

不要将`script`标签放到正文内容之前，除非你有特别的理由，因为它会阻塞页面的正常渲染。如果样式文件很大，那就需要将这个文件分割，将重要的样式声明首先加载，延迟加载其他的。虽然两个HTTP请求要明显比一个慢，但人对速度的感觉才是最重要的因素。

```html
<!-- 不好的 -->

<!doctype html>
<meta charset="utf-8">
<script src="analytics.js"></script>
<title>Hello, World.</title>
<p>...</p>

<!-- 推荐 -->

<!doctype html>
<meta charset="utf-8">
<title>Hello, world.</title>
<p>...</p>
<script src="analytics.js"></script>

```